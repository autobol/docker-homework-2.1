# Docker homework 2.1

## Objective 📋

Nginx container starts with the wrong index page. You need to add proper docker-compose.override.yaml to make it work

## Solution 👌

### You've need to investigate: 
- nginx configuration files
- Dockerfile
- docker-compose.yaml 

__IMPORTANT!__ The solution, should be based only on proper docker-compose.override.yaml configuration!

### To make the things work properly, please follow the manual below:

- Fork the project 
- Create proper docker-compose.override.yaml
- Commit and push the changes
- If the pipeline succeeds, you have completed the task
  
## Author 👀
Glukhov Aleksandr / DT IT DevOps School
